"""
Task 1
There is a list of arbitrary numbers, for example [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
Write a code that will remove (will not create a new one, namely, delete!)
from it all numbers that are less than 21 and more than 74.
# """
import copy

# Task 1

list1 = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44, 55]
list2 = copy(lst1)
print('list before removal: ', list1)
for i in range(len(list2)):
    print(i)
    if 21 < list2[i] < 74:
        list1.pop(i)
        print(list2)
print('list after removal: ', list1)
print()


# comprehension method
# print("Comprehension method:")
# list = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
# print('list before removal: ', list)
# list = [item for item in list if 21 < item < 74]
# print('list after removal: ', list)
# print()

"""
Task 2
There are two arbitrary numbers that are responsible for the minimum and maximum price. 
There is a Dict with store names and prices: 
{"cito": 47.999, "BB_studio" 42.999, "momo": 49.999,"main-service": 37.245, "buy.now": 38.324, 
"x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}. 
Write a code that will find and display the names of stores whose prices fall into the range
between the minimum and maximum prices.   
"""
# # Task 2112
# store_prices = {
#     "cito": 47.999,
#     "BB_studio": 42.999,
#     "momo": 49.999,
#     "main-service": 37.245,
#     "buy.now": 38.324,
#     "x-store": 37.166,
#     "the_partner": 38.988,
#     "store": 37.720,
#     "rozetka": 38.003
# }
#
# min_price = round(float(input("enter minimal price: ")), 3)
# max_price = round(float(input("enter maximal price: ")), 3)
# result = []
# for key, value in store_prices.items():
#     if min_price < value < max_price:
#         result.append(key)
# print("Match: " + ", ".join(result))
# print()
#
# # comprehension method
# print("Comprehension method:")
# min_price = round(float(input("enter minimal price: ")), 3)
# max_price = round(float(input("enter maximal price: ")), 3)
# result = [key for key, value in store_prices.items() if min_price < value < max_price]
# print("Match: " + ", ".join(result))
